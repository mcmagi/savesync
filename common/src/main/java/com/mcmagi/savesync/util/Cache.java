package com.mcmagi.savesync.util;

/**
 *
 * @author Michael C. Maggio
 */
public interface Cache<T>
{
	public T get();

	public void clear();
}
