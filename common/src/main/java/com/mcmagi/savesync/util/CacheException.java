package com.mcmagi.savesync.util;

/**
 *
 * @author Michael C. Maggio
 */
public class CacheException extends RuntimeException
{
	public CacheException(Throwable cause)
	{
		super(cause);
	}
}
