package com.mcmagi.savesync.util;

import java.io.File;
import java.io.IOException;
import java.nio.file.FileSystems;
import java.nio.file.FileVisitOption;
import java.nio.file.FileVisitResult;
import java.nio.file.FileVisitor;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.PathMatcher;
import java.nio.file.Paths;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.EnumSet;
import java.util.LinkedHashSet;
import java.util.Set;

/**
 * Performs a search in the specified path for filenames that match a glob
 * pattern.
 * @see java.nio.file.FileSystem#getPathMatcher
 * @author Michael C. Maggio
 */
public class FileScanner
{
	private final Path searchPath;
	private final Set<File> fileSet = new LinkedHashSet<>();

	/**
	 * Constructs a file scanner that will search in the specified path.
	 * @param searchPath String path
	 */
	public FileScanner(String searchPath)
	{
		this(Paths.get(searchPath));
	}

	/**
	 * Constructs a file scanner that will search in the specified path.
	 * @param searchPath Path
	 */
	public FileScanner(Path searchPath)
	{
		this.searchPath = searchPath;
	}

	/**
	 * Performs a scan for files with the specified glob pattern.
	 * @param pattern Glob pattern
	 * @throws IOException If there was an error during the scan
	 */
	public void scan(String pattern)
		throws IOException
	{
		Finder finder = new Finder(pattern);
		Files.walkFileTree(searchPath,
				EnumSet.noneOf(FileVisitOption.class), Integer.MAX_VALUE, finder);
	}

	/**
	 * Returns the set of files matched by this scanner.
	 * @return 
	 */
	public Set<File> getFiles()
	{
		return fileSet;
	}

	/**
	 * A FileVisitor implementation that matches files against the FileScanner's
	 * pattern.  Receives callbacks for each file and directory in the tree.
	 */
	private class Finder implements FileVisitor<Path>
	{
		private final PathMatcher pathMatcher;

		public Finder(String pattern)
		{
			// glob pattern must begin with a slash
			if (! pattern.startsWith("/"))
				pattern = "/" + pattern;

			this.pathMatcher = FileSystems.getDefault().getPathMatcher("glob:" + pattern);
		}

		@Override
		public FileVisitResult preVisitDirectory(Path dir, BasicFileAttributes attrs)
			throws IOException
		{
			return FileVisitResult.CONTINUE;
		}

		@Override
		public FileVisitResult visitFile(Path file, BasicFileAttributes attrs)
			throws IOException
		{
			if (pathMatcher.matches(getRelativePath(file)))
				fileSet.add(file.toFile());
			return FileVisitResult.CONTINUE;
		}

		@Override
		public FileVisitResult visitFileFailed(Path file, IOException exc)
			throws IOException
		{
			System.err.print(file + ": " + exc.getMessage());
			return FileVisitResult.CONTINUE;
		}

		@Override
		public FileVisitResult postVisitDirectory(Path dir, IOException exc)
			throws IOException
		{
			return FileVisitResult.CONTINUE;
		}

		/**
		 * Returns the path to the specified file relative to the FileScanner's searchPath.
		 * @param file Absolute path to file
		 * @return Relative path to file
		 */
		private Path getRelativePath(Path file)
		{
			return file.startsWith(searchPath) ?
					Paths.get(file.toString().substring(searchPath.toString().length())) :
					file;
		}
	}
}
