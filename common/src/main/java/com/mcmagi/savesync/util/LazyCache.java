package com.mcmagi.savesync.util;

/**
 *
 * @author Michael C. Maggio
 */
public class LazyCache<T> implements Cache
{
	private final Command<T> command;
	private volatile T value;

	public LazyCache(Command<T> command)
	{
		this.command = command;
	}

	@Override
	public T get()
	{
		if (value == null)
			load();

		return value;
	}

	@Override
	public void clear()
	{
		value = null;
	}

	public synchronized void load()
	{
		try
		{
			if (value == null)
				value = command.execute();
		}
		catch (Exception ex)
		{
			throw new CacheException(ex);
		}
	}
}
