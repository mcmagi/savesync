package com.mcmagi.savesync.util;

/**
 *
 * @author Michael C. Maggio
 */
public interface Command<T>
{
	public T execute() throws Exception;
}
