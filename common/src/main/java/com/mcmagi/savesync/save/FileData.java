package com.mcmagi.savesync.save;

import java.io.InputStream;
import java.util.Date;
import lombok.Data;

/**
 *
 * @author Michael C. Maggio
 */
@Data
public class FileData
{
	private final String name;
	private final long size;
	private final Date lastModified;
	private final InputStream stream;
}
