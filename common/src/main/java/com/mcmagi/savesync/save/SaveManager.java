package com.mcmagi.savesync.save;

import java.util.List;

/**
 *
 * @author Michael C. Maggio
 */
public interface SaveManager
{
	public void updateSavePackage(String gameId, FileData data) throws SaveException;

	public FileData getSavePackage(String gameId) throws SaveException;

	public List<SaveInfo> getSaveInfo() throws SaveException;
}
