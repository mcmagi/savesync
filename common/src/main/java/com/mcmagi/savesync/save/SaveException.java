package com.mcmagi.savesync.save;

/**
 * Represents an exception thrown by the SaveManager if a save cannot be sync'd.
 * @author Michael C. Maggio
 */
public class SaveException extends Exception
{
	public SaveException(String message, Throwable cause)
	{
		super(message + ": " + cause.getMessage(), cause);
	}
}
