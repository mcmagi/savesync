package com.mcmagi.savesync.save;

import com.fasterxml.jackson.annotation.JsonFormat;
import java.util.Date;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 *
 * @author Michael C. Maggio
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class SaveInfo
{
	public static final String ISO8601_DATE_PATTERN = "yyyy-MM-dd'T'HH:mm:ss.SSSZ";

	private String gameId;

	@JsonFormat(shape=JsonFormat.Shape.STRING, pattern=ISO8601_DATE_PATTERN, timezone="UTC")
	private Date updatedTime;
}
