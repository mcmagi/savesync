package com.mcmagi.savesync.profile;

import lombok.Data;

/**
 *
 * @author Michael C. Maggio
 */
@Data
public class ProfileMeta
{
	private String key;
	private Profile profile;

	public ProfileMeta()
	{
	}

	public ProfileMeta(String key, Profile profile)
	{
		this.key = key;
		this.profile = profile;
	}
}
