package com.mcmagi.savesync.profile;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

/**
 *
 * @author Michael C. Maggio
 */
public class Profiles
{
	private final Map<String,ProfileMeta> profileMap = new TreeMap<>();

	public Profiles(List<ProfileMeta> profiles)
	{
		for (ProfileMeta profile : profiles)
			profileMap.put(profile.getKey(), profile);
	}

	/**
	 * Returns the set of game ids in this profiles container.
	 * @return Set of game ids
	 */
	public Set<String> getGames()
	{
		return profileMap.keySet();
	}

	/**
	 * Returns the profile for the specified game id.
	 * @param gameId
	 * @return Profile
	 */
	public Profile getProfile(String gameId)
	{
		return profileMap.containsKey(gameId) ? profileMap.get(gameId).getProfile() : null;
	}

	/**
	 * Returns all profiles in this container.
	 * @return All profiles
	 */
	public List<ProfileMeta> getProfiles()
	{
		return new ArrayList<>(profileMap.values());
	}
}
