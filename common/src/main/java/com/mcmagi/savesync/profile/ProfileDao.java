package com.mcmagi.savesync.profile;

import java.io.IOException;
import java.util.List;

/**
 *
 * @author Michael C. Maggio
 */
public interface ProfileDao
{
	/**
	 * Fetches and returns all profiles backed by this dao.
	 * @return
	 * @throws ProfileException 
	 */
	public List<ProfileMeta> loadProfiles()
		throws ProfileException;
}
