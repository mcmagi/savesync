package com.mcmagi.savesync.profile;

import java.util.ArrayList;
import java.util.List;
import lombok.Data;

/**
 *
 * @author Michael C. Maggio
 */
@Data
public class Profile {
	private String name;
	private String subtitle;
	private String component;
	private final List<String> files = new ArrayList<>();
}
