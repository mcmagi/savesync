package com.mcmagi.savesync.profile;

/**
 * Represents an exception thrown by the ProfileManager if a profile cannot be located.
 * @author Michael C. Maggio
 */
public class ProfileException extends Exception
{
	public ProfileException(String message, Throwable cause)
	{
		super(message + ": " + cause.getMessage(), cause);
	}
}
