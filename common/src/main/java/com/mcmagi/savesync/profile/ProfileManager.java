package com.mcmagi.savesync.profile;

/**
 * The profile manager is used to retrieve savesync profile configuration for a
 * game.
 * @author Michael C. Maggio
 */
public interface ProfileManager
{
	/**
	 * Returns all profile data.
	 * @return All profile data.
	 */
	public Profiles getProfiles();
}
