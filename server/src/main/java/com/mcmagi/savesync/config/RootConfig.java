package com.mcmagi.savesync.config;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.mcmagi.savesync.profile.ProfileManager;
import com.mcmagi.savesync.save.SaveManager;
import com.mcmagi.savesync.profile.ProfileDao;
import com.mcmagi.savesync.service.ProfileDaoImpl;
import com.mcmagi.savesync.service.ProfileManagerImpl;
import com.mcmagi.savesync.service.SaveManagerImpl;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;
import org.springframework.scheduling.annotation.EnableScheduling;

/**
 *
 * @author Michael C. Maggio
 */
@Configuration
@PropertySource("classpath:savesync.properties")
@EnableScheduling
public class RootConfig
{
	@Bean
	public PropertySourcesPlaceholderConfigurer propertySourcesPlaceholderConfigurer()
	{
    	return new PropertySourcesPlaceholderConfigurer();
	}

	@Bean
	public SaveManager saveManager()
	{
		return new SaveManagerImpl();
	}

	@Bean
	public ProfileManager profileManager()
	{
		return new ProfileManagerImpl();
	}

	@Bean
	public ProfileDao profileDao()
	{
		return new ProfileDaoImpl();
	}

	@Bean
	public ObjectMapper objectMapper()
	{
		return new ObjectMapper();
	}
}
