package com.mcmagi.savesync.config;

import com.mcmagi.savesync.web.ProfileController;
import com.mcmagi.savesync.web.SaveController;
import org.apache.commons.io.FileUtils;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.multipart.MultipartResolver;
import org.springframework.web.multipart.commons.CommonsMultipartResolver;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

/**
 *
 * @author Michael C. Maggio
 */
@Configuration
@EnableWebMvc
public class ServletConfig extends WebMvcConfigurerAdapter
{
	private static final long MAX_FILE_SIZE = 100 * FileUtils.ONE_MB;

	@Bean
	public SaveController saveController()
	{
		return new SaveController();
	}

	@Bean
	public ProfileController profileController()
	{
		return new ProfileController();
	}

	@Bean
	public MultipartResolver multipartResolver()
	{
		CommonsMultipartResolver resolver = new CommonsMultipartResolver();
		resolver.setMaxUploadSize(MAX_FILE_SIZE);
		return resolver;
	}
}
