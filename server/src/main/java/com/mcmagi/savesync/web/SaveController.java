package com.mcmagi.savesync.web;

import com.mcmagi.savesync.save.FileData;
import com.mcmagi.savesync.save.SaveException;
import com.mcmagi.savesync.save.SaveInfo;
import com.mcmagi.savesync.save.SaveManager;
import java.io.IOException;
import java.util.Date;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.format.annotation.DateTimeFormat.ISO;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.multipart.MultipartFile;

/**
 *
 * @author Michael C. Maggio
 */
@Controller
@RequestMapping("/save")
public class SaveController
{
	@Autowired
	private SaveManager saveManager;

	@RequestMapping(value="/", method=RequestMethod.GET)
	@ResponseBody
	public List<SaveInfo> list()
		throws SaveException
	{
		return saveManager.getSaveInfo();
	}

	@RequestMapping(value="/{gameId}", method=RequestMethod.GET)
	@ResponseBody
	public InputStreamResource fetch(@PathVariable("gameId") String gameId)
		throws SaveException
	{
		final FileData data = saveManager.getSavePackage(gameId);

		// NOTE: spring 3.2.8 has a bug such that it will attempt to read the stream
		// twice (once for the length and again for the content) but fail on the
		// second time.  Overriding the methods with the info we have works arround
		// this issue.
		return new InputStreamResource(data.getStream()) {
			@Override public String getFilename() {
				return data.getName();
			}
			@Override public long contentLength() {
				return data.getSize();
			}
			@Override public long lastModified() {
				return data.getLastModified().getTime();
			}
		};
	}

	@RequestMapping(value="/{gameId}", method=RequestMethod.POST)
	@ResponseStatus(HttpStatus.OK)
	public void update(@PathVariable("gameId") String gameId,
			@RequestParam("file") MultipartFile file,
			@RequestParam("lastModified") @DateTimeFormat(iso=ISO.DATE_TIME) Date lastModified)
		throws SaveException, IOException
	{
		saveManager.updateSavePackage(gameId, new FileData(file.getName(), file.getSize(), lastModified, file.getInputStream()));
	}
}
