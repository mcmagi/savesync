package com.mcmagi.savesync.web;

import com.mcmagi.savesync.profile.Profile;
import com.mcmagi.savesync.profile.ProfileMeta;
import com.mcmagi.savesync.service.ProfileManagerImpl;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 *
 * @author Michael C. Maggio
 */
@RequestMapping("/profile")
public class ProfileController
{
	@Autowired
	private ProfileManagerImpl profileManager;

	@RequestMapping(value="/", method=RequestMethod.GET)
	@ResponseBody
	public List<ProfileMeta> getProfiles(
		@RequestParam(value="refresh", required=false, defaultValue="false") boolean refresh)
	{
		if (refresh)
			profileManager.reload();

		return profileManager.getProfiles().getProfiles();
	}

	@RequestMapping(value="/{gameId}", method=RequestMethod.GET)
	@ResponseBody
	public Profile getProfile(String gameId)
	{
		return profileManager.getProfiles().getProfile(gameId);
	}
}
