package com.mcmagi.savesync.service;

import com.mcmagi.savesync.profile.ProfileDao;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.mcmagi.savesync.profile.Profile;
import com.mcmagi.savesync.profile.ProfileException;
import com.mcmagi.savesync.profile.ProfileMeta;
import com.mcmagi.savesync.util.FileScanner;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;

/**
 *
 * @author Michael C. Maggio
 */
public class ProfileDaoImpl implements ProfileDao
{
	private static final String PROFILE_EXT = ".json";

	@Autowired
	private ObjectMapper objectMapper;

	@Value("${profile.path}")
	private String profilePath;

	@Override
	public List<ProfileMeta> loadProfiles()
		throws ProfileException
	{
		try
		{
			List<ProfileMeta> profiles = new ArrayList<>();
			for (File file : getProfileFiles())
			{
				ProfileMeta profile = new ProfileMeta();
				profile.setKey(getGameId(file));
				profile.setProfile(getProfile(file));
				profiles.add(profile);
			}
			return profiles;
		}
		catch (IOException ex)
		{
			throw new ProfileException("Could not read profile data from " + profilePath, ex);
		}
	}

	private Set<File> getProfileFiles()
		throws IOException
	{
		FileScanner scanner = new FileScanner(profilePath);
		scanner.scan("*" + PROFILE_EXT);
		return scanner.getFiles();
	}

	private String getGameId(File file)
	{
		String filename = file.getName();
		return filename.substring(0, filename.indexOf(PROFILE_EXT));
	}

	private Profile getProfile(File file)
		throws IOException 
	{
		return objectMapper.readValue(file, Profile.class);
	}
}
