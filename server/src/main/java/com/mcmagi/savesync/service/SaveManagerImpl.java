package com.mcmagi.savesync.service;

import com.mcmagi.savesync.save.FileData;
import com.mcmagi.savesync.save.SaveException;
import com.mcmagi.savesync.save.SaveManager;
import com.mcmagi.savesync.save.SaveInfo;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.attribute.FileTime;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.io.IOCase;
import org.apache.commons.io.IOUtils;
import org.apache.commons.io.filefilter.FalseFileFilter;
import org.apache.commons.io.filefilter.WildcardFileFilter;
import org.springframework.beans.factory.annotation.Value;

/**
 *
 * @author Michael C. Maggio
 */
public class SaveManagerImpl implements SaveManager
{
	private final static String EXTENSION = ".zip";
	private final static String DEFAULT_FOLDER = "default";

	@Value("${file.store.path}")
	private String fileStorePath;

	@Override
	public void updateSavePackage(String gameId, FileData data)
		throws SaveException
	{
		// TODO: use extension from FileData?
		File pkgFile = new File(getPackageFilePath(gameId));

		// ensure parent directory exists
		pkgFile.getParentFile().mkdirs();

		try
		{
			pkgFile.createNewFile();

			try (OutputStream outStream = new FileOutputStream(pkgFile))
			{
				// copy to output stream
				IOUtils.copy(data.getStream(), outStream);
			}

			Files.setLastModifiedTime(pkgFile.toPath(), FileTime.fromMillis(data.getLastModified().getTime()));
		}
		catch (IOException ex)
		{
			throw new SaveException("failed to update package for game " + gameId, ex);
		}

		pkgFile.setLastModified(data.getLastModified().getTime());
	}

	@Override
	public FileData getSavePackage(String gameId)
		throws SaveException
	{
		File pkgFile = new File(getPackageFilePath(gameId));
		try
		{
			return new FileData(getPackageFileName(gameId), pkgFile.length(),
					new Date(pkgFile.lastModified()), new FileInputStream(pkgFile));
		}
		catch (FileNotFoundException ex)
		{
			// TODO: consider how we could report this as a 404
			throw new SaveException("could not find file " + pkgFile, ex);
		}
	}

	@Override
	public List<SaveInfo> getSaveInfo()
		throws SaveException
	{
		File fileStore = new File(getPackagePath());
		if (! fileStore.exists())
			return Collections.<SaveInfo>emptyList();

		Collection<File> files = FileUtils.listFiles(fileStore,
				new WildcardFileFilter("*" + EXTENSION, IOCase.INSENSITIVE),
				FalseFileFilter.INSTANCE);

		List<SaveInfo> list = new ArrayList<>();
		for (File file : files)
		{
			SaveInfo info = new SaveInfo(
					getGameId(file.getName()), new Date(file.lastModified())
			);
			list.add(info);
		}
		return list;
	}

	private String getPackagePath()
	{
		// in the future we want to support different "stahes" of save games
		// so for now we always store everything in a "default" folder
		return fileStorePath + File.separatorChar + DEFAULT_FOLDER;
	}

	private String getPackageFilePath(String gameId)
	{
		return getPackagePath() + File.separatorChar + getPackageFileName(gameId);
	}

	private String getPackageFileName(String gameId)
	{
		return gameId + EXTENSION;
	}

	private String getGameId(String filename)
	{
		return FilenameUtils.getBaseName(filename);
	}
}
