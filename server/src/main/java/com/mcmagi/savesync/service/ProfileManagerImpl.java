package com.mcmagi.savesync.service;

import com.mcmagi.savesync.profile.ProfileDao;
import com.mcmagi.savesync.profile.ProfileManager;
import com.mcmagi.savesync.profile.Profiles;
import com.mcmagi.savesync.util.Cache;
import com.mcmagi.savesync.util.Command;
import com.mcmagi.savesync.util.LazyCache;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;

/**
 *
 * @author Michael C. Maggio
 */
public class ProfileManagerImpl implements ProfileManager
{
	private static final long DELAY = 60 * 1000; // reload every minute

	@Autowired
	private ProfileDao profileDao;

	private final Cache<Profiles> profileCache = new LazyCache<>(new Command<Profiles>() {
		@Override public Profiles execute() throws Exception {
			return new Profiles(profileDao.loadProfiles());
		}
	});

	@Override
	public Profiles getProfiles()
	{
		return profileCache.get();
	}

	@Scheduled(fixedDelay=DELAY)
	public void reload() {
		profileCache.clear();
	}
}
