package com.mcmagi.savesync.client.http;

import com.mcmagi.savesync.save.FileData;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.text.DateFormat;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import org.apache.commons.io.IOUtils;
import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.HttpHeaders;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.client.methods.RequestBuilder;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.entity.mime.content.InputStreamBody;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicNameValuePair;

/**
 *
 * @author Michael C. Maggio
 */
public class RestHttpClient
{
	private static final int STATUS_SUCCESS = 200;

	private CloseableHttpClient httpClient;

	@PostConstruct
	public void init()
	{
		httpClient = HttpClients.createDefault();
	}

	@PreDestroy
	public void destroy()
	{
		IOUtils.closeQuietly(httpClient);
	}

	public InputStream get(URI url)
		throws IOException, RequestFailedException
	{
		return sendRequest(new HttpGet(url)).getInputStream();
	}

	public FileData getFile(URI url)
		throws IOException, RequestFailedException
	{
		return asFileData(sendRequest(new HttpGet(url)), url);
	}

	public InputStream post(URI url, Map<String,String> params, String fileParamName, final FileData data)
		throws IOException, RequestFailedException
	{
		HttpEntity fileEntity = MultipartEntityBuilder.create()
				.addPart(fileParamName, new InputStreamBody(data.getStream(), ContentType.DEFAULT_BINARY, data.getName()) {
					@Override public long getContentLength() {
						return data.getSize();
					}
				})
				.build();

		HttpUriRequest post = RequestBuilder.post()
				.setUri(url)
				.addParameters(buildNameValuePairs(params))
				.setEntity(fileEntity)
				.build();

		return sendRequest(post).getInputStream();
	}

	/**
	 * Sends the request to the server.
	 * @param request
	 * @return
	 * @throws IOException
	 * @throws RequestFailedException 
	 */
	private RestResponse sendRequest(HttpUriRequest request)
		throws IOException, RequestFailedException
	{
		try (CloseableHttpResponse response = httpClient.execute(request))
		{
			int statusCode = response.getStatusLine().getStatusCode();
			if (statusCode != STATUS_SUCCESS)
				throw new RequestFailedException(request.getURI(), statusCode);

			return asRestResponse(response);
		}
	}

	private NameValuePair[] buildNameValuePairs(Map<String,String> paramMap)
	{
		List<NameValuePair> nvpList = new ArrayList<>();
		for (Map.Entry<String,String> entry : paramMap.entrySet())
			nvpList.add(new BasicNameValuePair(entry.getKey(), entry.getValue()));
		return nvpList.toArray(new NameValuePair[0]);
	}

	private FileData asFileData(RestResponse response, URI uri)
		throws IOException
	{
		DateFormat dateFormat = DateFormat.getDateTimeInstance();
		Header lastModifiedHdr = response.getHeader(HttpHeaders.LAST_MODIFIED);
		Date date = null;
		
		if (lastModifiedHdr != null && lastModifiedHdr.getValue() != null)
		{
			try
			{
				dateFormat.parse(lastModifiedHdr.getValue());
			}
			catch (ParseException ex)
			{
				// not a big deal, we can safely ignore this
			}
		}

		String[] path = uri.getPath().split("/");
		String fileName = path[path.length-1];

		return new FileData(fileName, response.getContentLength(), date, response.getInputStream());
	}

	private static RestResponse asRestResponse(HttpResponse response)
		throws IOException
	{
		HttpEntity entity = response.getEntity();
		byte[] data = new byte[0];
		if (entity != null)
		{
			try (InputStream content = entity.getContent())
			{
				if (entity.getContentLength() >= 0)
					data = IOUtils.toByteArray(content, entity.getContentLength());
				else
					data = IOUtils.toByteArray(content);
			}
		}

		return new RestResponse(response.getAllHeaders(), data);
	}

	private static class RestResponse
	{
		private final Map<String,List<Header>> headers = new HashMap<>();
		private final byte[] content;

		/**
		 * Rest response constructor.
		 * @param headers
		 * @param contentLength
		 * @param content 
		 */
		public RestResponse(Header[] headers, byte[] data)
		{
			this.headers.putAll(indexHeaders(headers));
			this.content = data;
		}

		public List<Header> getHeaders(String name)
		{
			return headers.get(name);
		}

		public Header getHeader(String name)
		{
			return headers.containsKey(name) ? headers.get(name).get(0) : null;
		}

		public long getContentLength()
		{
			return content.length;
		}

		public byte[] getContent()
		{
			return content;
		}

		public InputStream getInputStream()
		{
			return new ByteArrayInputStream(content);
		}

		private static Map<String,List<Header>> indexHeaders(Header[] headers)
		{
			Map<String,List<Header>> headerMap = new HashMap<>();
			for (Header h : headers)
			{
				if (! headerMap.containsKey(h.getName()))
					headerMap.put(h.getName(), new ArrayList<Header>());
				headerMap.get(h.getName()).add(h);
			}
			return headerMap;
		}
	}
}
