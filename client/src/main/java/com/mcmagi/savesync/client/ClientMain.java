package com.mcmagi.savesync.client;

import com.mcmagi.savesync.save.SaveException;
import com.mcmagi.savesync.client.model.ClientConfig;
import com.mcmagi.savesync.save.SaveInfo;
import com.mcmagi.savesync.save.SaveManager;
import com.mcmagi.savesync.client.config.RootConfig;
import com.mcmagi.savesync.client.dao.ClientConfigDao;
import java.io.IOException;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

/**
 *
 * @author Michael C. Maggio
 */
public class ClientMain
{
	/*
		Future enhancements:
		* correctness: unpack-delete-move instead of unpack in place
		* feature: multiple save stashes
		* refactor: should we wrap IOException? (similar to SaveException)
		* security: http auth
		* testing: unit tests
		* duplication: both ProfileManagerImpl's are similar; do we need both?
		* duplication: import triforce for cache stuff
	*/

	@Autowired
	private ClientConfigDao configDao;

	@Autowired
	private ClientConfig config;

	@Autowired
	private SaveManager saveManager;

	@Autowired
	private GameDispatcher dispatcher;

	@Autowired
	private OptionsParser optionsParser;

	/**
	 * Performs a sync of saved games with the server.
	 * @throws Exception 
	 */
	public void sync(String[] s) throws Exception
	{
		// load configuration and save to singleton
		config.configure(configDao.loadClientConfig());

		Options opts = optionsParser.parseOptions(s);

		// query server for remote save info
		Map<String,Date> remoteUpdateTimeMap = buildDateIndex(saveManager.getSaveInfo());

		Set<String> games = getGames(opts);

		// process each local game in client config
		for (String gameId : games)
			processGame(gameId, remoteUpdateTimeMap.get(gameId), opts);
	}

	private void processGame(String gameId, Date remoteUpdateTime, Options options)
	{
		try
		{
			dispatcher.dispatch(gameId, config.getGames().get(gameId), remoteUpdateTime, options);
		}
		catch (IOException|SaveException ex)
		{
			// we're not using a logger atm, so...
			System.err.println("Could not sync game: " + gameId);
			ex.printStackTrace();
		}
	}

	/**
	 * Returns a map of last update times indexed by game identifier.
	 * @param saveInfoList List of save info data
	 * @return Map of gameId-to-updateTime
	 */
	private Map<String,Date> buildDateIndex(List<SaveInfo> saveInfoList)
	{
		Map<String,Date> saveInfoMap = new HashMap<>();
		for (SaveInfo saveInfo : saveInfoList)
			saveInfoMap.put(saveInfo.getGameId(), saveInfo.getUpdatedTime());
		return saveInfoMap;
	}

	/**
	 * Returns the set of games to operate on for the given configuration.
	 * @param opts
	 * @return 
	 */
	private Set<String> getGames(Options opts)
	{
		if (opts.isAll())
			return config.getGames().keySet();

		// ensure we filter by game ids in configuration
		Set<String> gameIds = new LinkedHashSet<>();
		for (String gameId : opts.getGames())
		{
			if (config.getGames().containsKey(gameId))
				gameIds.add(gameId);
			else
				System.err.println("skipping " + gameId + ": no profile found");
		}
			
		return gameIds;
	}

	public static void main(String[] s) throws Exception
	{
		ApplicationContext ctx = new AnnotationConfigApplicationContext(RootConfig.class);
		ClientMain main = ctx.getBean(ClientMain.class);
		main.sync(s);
	}
}
