package com.mcmagi.savesync.client.pack;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.attribute.FileTime;
import java.util.Collection;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;
import java.util.zip.ZipOutputStream;
import org.apache.commons.io.IOUtils;

/**
 *
 * @author Michael C. Maggio
 */
public class ZipPackageManagerImpl implements PackageManager
{
	public static final String FILE_TYPE_ZIP = "zip";

	@Override
	public File packFiles(File destFile, File sourcePath, Collection<File> sourceFileList)
		throws IOException
	{
		// create the destination zip file
		destFile.createNewFile();
		try (ZipOutputStream zipStream = new ZipOutputStream(new FileOutputStream(destFile)))
		{
			for (File sourceFile : sourceFileList)
			{
				if (sourceFile.isFile()) // normal files only
				{
					zipStream.putNextEntry(toZipEntry(sourcePath, sourceFile));
					// copy the contents of the file to the zip stream
					try (FileInputStream sourceStream = new FileInputStream(sourceFile))
					{
						IOUtils.copy(sourceStream, zipStream);
					}
					zipStream.closeEntry();
				}
			}
		}

		return destFile;
	}

	@Override
	public void unpackFiles(String destPath, InputStream sourceStream)
		throws IOException
	{
		ZipInputStream zipStream = new ZipInputStream(sourceStream);
		ZipEntry entry = zipStream.getNextEntry();
		while (entry != null)
		{
			File destFile = new File(destPath + File.separator + entry.getName());

			// ensure containing directory exists
			destFile.getParentFile().mkdirs();

			if (! entry.isDirectory())
			{
				destFile.createNewFile();
				try (FileOutputStream destStream = new FileOutputStream(destFile))
				{
					// will copy to the end of the current zip entry
					IOUtils.copy(zipStream, destStream);
				}
				Files.setLastModifiedTime(destFile.toPath(), entry.getLastModifiedTime());
			}

			// advance to next entry
			zipStream.closeEntry();
			entry = zipStream.getNextEntry();
		}
	}

	@Override
	public String getFileType()
	{
		return FILE_TYPE_ZIP;
	}

	private ZipEntry toZipEntry(File sourcePath, File f)
	{
		// get filename relative to source path
		String entryName = f.getPath().substring(sourcePath.getPath().length());
		if (entryName.startsWith(File.separator))
			entryName = entryName.substring(1);

		ZipEntry entry = new ZipEntry(entryName);
		entry.setLastModifiedTime(FileTime.fromMillis(f.lastModified()));
		return entry;
	}
}
