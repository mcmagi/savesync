package com.mcmagi.savesync.client;

import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import org.apache.commons.lang3.StringUtils;

/**
 *
 * @author Michael C. Maggio
 */
public class OptionsParser
{
	public static final String PREFIX = "-";

	@RequiredArgsConstructor
	public enum Option 
	{
		CLEAN("clean"),
		TEST("test"),
		ALL("all"),
		GAME("game"),
		UP("up"),
		DOWN("down");

		private static final Map<String,Option> valueMap = new HashMap<>();

		static
		{
			for (Option o : values())
				valueMap.put(o.name, o);
		}

		@Getter
		private final String name;

		public String getOption()
		{
			return PREFIX + name;
		}

		public static Option findByName(String name)
		{
			return valueMap.get(name);
		}
	}

	/**
	 * Parses the options array and returns a representative Options object.
	 * @param args Command line args
	 * @return
	 * @throws OptionsException
	 */
	public Options parseOptions(String[] args) throws OptionsException
	{
		Options o = new Options();

		for (int i = 0; i < args.length; i++)
		{
			String optStr = args[i];
			if (optStr.startsWith(PREFIX))
			{
				String optName = optStr.substring(PREFIX.length());
				Option opt = Option.findByName(optName);

				if (opt == null)
					throw new OptionsException(optName, "Unrecognized option");

				switch (opt)
				{
				case CLEAN:
					o.setClean(true);
					break;
				case TEST:
					o.setTest(true);
					break;
				case ALL:
					o.setAll(true);
					break;
				case GAME:
					if (i == args.length-1)
						throw new OptionsException(opt.getOption(), "Must specify a game id");
					String gameStr = args[++i];
					List<String> gameList = gameStr.contains(",") ?
						Arrays.asList(StringUtils.split(gameStr, ",")) :
						Collections.singletonList(gameStr);
					o.getGames().addAll(gameList);
					break;
				case UP:
					if (o.getDirection() != null)
						throw new OptionsException(opt.getOption(), "Cannot specify with " + Option.DOWN.getOption());
					o.setDirection(Options.Direction.UP);
					break;
				case DOWN:
					if (o.getDirection() != null)
						throw new OptionsException(opt.getOption(), "Cannot specify with " + Option.UP.getOption());
					o.setDirection(Options.Direction.DOWN);
				}
			}
			else
				throw new OptionsException(optStr, "Not an option");
		}

		// default to -all if neither -all or -game are given
		if (! o.isAll() && o.getGames().isEmpty())
			o.setAll(true);

		// print a warning if -clean is given
		if (o.isClean())
			System.out.println(Option.CLEAN.getOption() + " is not currently implemented");

		return o;
	}
}
