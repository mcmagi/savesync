package com.mcmagi.savesync.client.model;

import java.net.URI;
import java.util.Map;
import java.util.TreeMap;
import lombok.Data;

/**
 *
 * @author Michael C. Maggio
 */
@Data
public class ClientConfig
{
	private URI server;
	private final Map<String,String> games = new TreeMap<>();

	/**
	 * Sets this object's configuration to the supplied values.
	 * @param config Supplied value
	 */
	public void configure(ClientConfig config)
	{
		server = config.server;
		games.clear();
		games.putAll(config.games);
	}
}
