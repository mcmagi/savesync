package com.mcmagi.savesync.client.http;

import com.mcmagi.savesync.profile.ProfileDao;
import com.mcmagi.savesync.profile.ProfileException;
import com.mcmagi.savesync.profile.ProfileManager;
import com.mcmagi.savesync.profile.Profiles;
import com.mcmagi.savesync.util.Cache;
import com.mcmagi.savesync.util.Command;
import com.mcmagi.savesync.util.LazyCache;
import org.springframework.beans.factory.annotation.Autowired;

/**
 *
 * @author Michael C. Maggio
 */
public class ProfileManagerHttpClient implements ProfileManager
{
	@Autowired
	private ProfileDao profileDao;

	private final Cache<Profiles> profiles = new LazyCache<>(new Command<Profiles>() {
		@Override public Profiles execute() throws ProfileException {
			return new Profiles(profileDao.loadProfiles());
		}
	});

	@Override
	public Profiles getProfiles()
	{
		return profiles.get();
	}
}
