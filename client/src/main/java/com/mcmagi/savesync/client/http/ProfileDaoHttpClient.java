package com.mcmagi.savesync.client.http;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.mcmagi.savesync.client.model.ClientConfig;
import com.mcmagi.savesync.profile.ProfileDao;
import com.mcmagi.savesync.profile.ProfileException;
import com.mcmagi.savesync.profile.ProfileMeta;
import com.mcmagi.savesync.profile.Profiles;
import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.util.UriComponentsBuilder;

/**
 *
 * @author Michael C. Maggio
 */
public class ProfileDaoHttpClient implements ProfileDao
{
	@Autowired
	private ObjectMapper mapper;

	@Autowired
	private RestHttpClient restClient;

	@Autowired
	private ClientConfig config;

	@Override
	public List<ProfileMeta> loadProfiles()
		throws ProfileException
	{
		try (InputStream stream = restClient.get(getProfilePath()))
		{
			return mapper.readValue(stream, new TypeReference<List<ProfileMeta>>(){});
		}
		catch (JsonProcessingException ex)
		{
			throw new ProfileException("Failed to parse server response; profile data expected", ex);
		}
		catch (IOException|RequestFailedException ex)
		{
			throw new ProfileException("Failed obtain list of profiles from server", ex);
		}
	}

	private URI getProfilePath()
	{
		URI uri = UriComponentsBuilder.fromUri(config.getServer())
				.pathSegment("profile")
				.build().toUri();
		try
		{
			// hack fix to add trailing slash
			return new URI(uri.toString() + "/");
		}
		catch (URISyntaxException ex)
		{
			throw new RuntimeException("could not build uri", ex);
		}
	}
}
