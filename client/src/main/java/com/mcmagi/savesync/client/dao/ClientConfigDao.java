package com.mcmagi.savesync.client.dao;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.mcmagi.savesync.client.model.ClientConfig;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;

/**
 * Loads the client-config.json file from within the classpath.
 * @author Michael C. Maggio
 */
public class ClientConfigDao
{
	private static final String RESOURCE_NAME = "client-config.json";

	private final ObjectMapper mapper = new ObjectMapper();

	/**
	 * Loads and deserializes the client config.
	 * @return Client config
	 * @throws IOException 
	 */
	public ClientConfig loadClientConfig()
		throws IOException
	{
		return mapper.readValue(getProfileResource(), ClientConfig.class);
	}

	/**
	 * Returns the client config file as an input stream.
	 * @return Input stream
	 * @throws FileNotFoundException 
	 */
	private InputStream getProfileResource()
		throws FileNotFoundException
	{
		return new FileInputStream(RESOURCE_NAME);
	}
}
