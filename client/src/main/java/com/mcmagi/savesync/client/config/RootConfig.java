package com.mcmagi.savesync.client.config;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.mcmagi.savesync.save.SaveManager;
import com.mcmagi.savesync.client.ClientMain;
import com.mcmagi.savesync.client.GameDispatcher;
import com.mcmagi.savesync.client.OptionsParser;
import com.mcmagi.savesync.client.dao.ClientConfigDao;
import com.mcmagi.savesync.client.http.ProfileDaoHttpClient;
import com.mcmagi.savesync.client.http.ProfileManagerHttpClient;
import com.mcmagi.savesync.client.http.RestHttpClient;
import com.mcmagi.savesync.client.http.SaveManagerHttpClient;
import com.mcmagi.savesync.client.model.ClientConfig;
import com.mcmagi.savesync.client.pack.PackageManager;
import com.mcmagi.savesync.client.pack.ZipPackageManagerImpl;
import com.mcmagi.savesync.profile.ProfileDao;
import com.mcmagi.savesync.profile.ProfileManager;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 *
 * @author Michael C. Maggio
 */
@Configuration
public class RootConfig
{
	@Bean
	public ObjectMapper objectMapper()
	{
		return new ObjectMapper(); // jackson object mapper
	}

	@Bean
	public ClientConfigDao clientConfigDao()
	{
		return new ClientConfigDao();
	}

	@Bean
	public ProfileDao profileDao()
	{
		return new ProfileDaoHttpClient();
	}

	@Bean
	public ClientConfig clientConfig()
	{
		return new ClientConfig();
	}

	@Bean
	public SaveManager saveManager()
	{
		return new SaveManagerHttpClient();
	}

	@Bean
	public ProfileManager profileManager()
	{
		return new ProfileManagerHttpClient();
	}

	@Bean
	public RestHttpClient httpClient()
	{
		return new RestHttpClient();
	}

	@Bean
	public PackageManager zipPackageManager()
	{
		return new ZipPackageManagerImpl();
	}

	@Bean
	public GameDispatcher gameDispatcher()
	{
		return new GameDispatcher();
	}

	@Bean
	public ClientMain clientMain()
	{
		return new ClientMain();
	}

	@Bean
	public OptionsParser optionsParser()
	{
		return new OptionsParser();
	}
}
