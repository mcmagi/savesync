package com.mcmagi.savesync.client.http;

import java.net.URI;

/**
 *
 * @author Michael C. Maggio
 */
public class RequestFailedException extends Exception
{
	private final int statusCode;
	private final URI uri;

	public RequestFailedException(URI uri, int statusCode)
	{
		super("Request to " + uri + " failed with status code " + statusCode);
		this.statusCode = statusCode;
		this.uri = uri;
	}

	public int getStatusCode()
	{
		return statusCode;
	}

	public URI getUri()
	{
		return uri;
	}
}
