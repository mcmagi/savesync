package com.mcmagi.savesync.client;

import com.mcmagi.savesync.util.FileScanner;
import com.mcmagi.savesync.save.FileData;
import com.mcmagi.savesync.save.SaveException;
import com.mcmagi.savesync.save.SaveManager;
import com.mcmagi.savesync.profile.Profile;
import com.mcmagi.savesync.client.pack.PackageManager;
import com.mcmagi.savesync.profile.ProfileManager;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.springframework.beans.factory.annotation.Autowired;

/**
 *
 * @author Michael C. Maggio
 */
public class GameDispatcher
{
	@Autowired
	private ProfileManager profileManager;

	@Autowired
	private PackageManager packageManager;

	@Autowired
	private SaveManager saveManager;

	public void dispatch(String gameId, String localPath, Date remoteUpdateTime, Options options) // TODO: something better than passing the whole Options object?
		throws IOException, SaveException
	{
		Profile profile = profileManager.getProfiles().getProfile(gameId);
		if (profile == null)
		{
			System.out.println(gameId + " *** WARNING: no profile found");
			return;
		}

		Collection<File> files = findMatchingFiles(localPath, profile.getFiles());

		// if there are no files locally or remotely, then there's nothing to sync
		if ((options.getDirection() == null && remoteUpdateTime == null && files.isEmpty()) ||
			(options.getDirection() == Options.Direction.UP && files.isEmpty()) ||
			(options.getDirection() == Options.Direction.DOWN && remoteUpdateTime == null))
		{
			System.out.println(gameId + " --- no syncing needed: no save files found");
			return;
		}

		Date localUpdateTime = truncateToSeconds(getLastUpdateTime(files));
		remoteUpdateTime = truncateToSeconds(remoteUpdateTime);

		Options.Direction direction = options.getDirection();
		if (direction == null)
			direction = getDirection(localUpdateTime, remoteUpdateTime);

		if (direction == Options.Direction.UP)
		{
			System.out.println(gameId + " --> syncing to server");

			if (options.isTest())
				return;

			// pack local files and send package to server
			File pkgFile = packageManager.packFiles(getPackageFile(gameId), new File(localPath), files);
			try (InputStream pkgStream = new FileInputStream(pkgFile))
			{
				saveManager.updateSavePackage(gameId, new FileData(
						FilenameUtils.getName(pkgFile.getName()), pkgFile.length(),
						localUpdateTime, pkgStream));
			}

			// cleanup package file when done
			pkgFile.delete();
		}
		else if (direction == Options.Direction.DOWN)
		{
			System.out.println(gameId + " <-- syncing from server");

			if (options.isTest())
				return;

			// get latest package from server and unpack
			FileData data = saveManager.getSavePackage(gameId);
			try (InputStream pkgStream = data.getStream())
			{
				// TODO:
				// The way this is written, it will not clean up files that meet
				// the pattern but are not replaced, so ideally we want to delete
				// all local files before unpacking.  But, we want to ensure that
				// both the server request and the unpack operation are also
				// successful first.  The best way to do this would be to unpack
				// to a temp directory, delete the local files, then move the
				// package contents to the localPath.
				packageManager.unpackFiles(localPath, pkgStream);
			}
		}
		else
			System.out.println(gameId + " --- no syncing needed: up to date");
	}

	private Collection<File> findMatchingFiles(String path, List<String> filePatterns)
		throws IOException
	{
		FileScanner scanner = new FileScanner(path);
		for (String pattern : filePatterns)
			scanner.scan(pattern);
		return scanner.getFiles();
	}

	private Date getLastUpdateTime(Collection<File> files)
	{
		long lastUpdateTime = 0;
		for (File f : files)
		{
			if (f.lastModified() > lastUpdateTime)
				lastUpdateTime = f.lastModified();
		}

		return new Date(lastUpdateTime);
	}

	/**
	 * Truncates a date's precision to seconds by zeroing out the milliseconds field.
	 * @param date
	 * @return 
	 */
	private static Date truncateToSeconds(Date date)
	{
		if (date == null)
			return null;

		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		cal.set(Calendar.MILLISECOND, 0);
		return cal.getTime();
	}

	/**
	 * Determines the sync direction based on the remote/local last update times.
	 * @param localUpdateTime Update time of local package
	 * @param remoteUpdateTime Update time of remote package
	 * @return Direction
	 */
	private Options.Direction getDirection(Date localUpdateTime, Date remoteUpdateTime)
	{
		return remoteUpdateTime == null || localUpdateTime.after(remoteUpdateTime) ? Options.Direction.UP :
			localUpdateTime.before(remoteUpdateTime) ? Options.Direction.DOWN :
			null;
	}

	private File getPackageFile(String gameId)
	{
		return new File(FileUtils.getTempDirectoryPath() + File.separatorChar + gameId + FilenameUtils.EXTENSION_SEPARATOR_STR + packageManager.getFileType());
	}
}
