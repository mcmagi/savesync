package com.mcmagi.savesync.client.pack;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.Collection;

/**
 *
 * @author Michael C. Maggio
 */
public interface PackageManager {
	public File packFiles(File destFileName, File sourcePath, Collection<File> sourceFileList)
		throws IOException;

	public void unpackFiles(String destPath, InputStream sourceStream)
		throws IOException;

	public String getFileType();
}
