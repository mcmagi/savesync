package com.mcmagi.savesync.client;

import java.util.LinkedHashSet;
import java.util.Set;
import lombok.Data;

/**
 *
 * @author Michael C. Maggio
 */
@Data
public class Options
{
	public enum Direction { UP, DOWN }

	/**
	 * Indicates that local files should be cleaned up before synced locally.
	 */
	private boolean clean = false;

	/**
	 * Reports what the savesync client will do but does not actually perform a sync.
	 */
	private boolean test = false;

	/**
	 * Indicates that all games in the configuration should be synced.
	 */
	private boolean all = false;

	/**
	 * A specific game id to sync.
	 */
	private final Set<String> games = new LinkedHashSet<>();

	/**
	 * Direction override.  Default is to rely on timestamps.
	 */
	private Direction direction;
}
