package com.mcmagi.savesync.client.http;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.mcmagi.savesync.save.FileData;
import com.mcmagi.savesync.save.SaveException;
import com.mcmagi.savesync.save.SaveInfo;
import com.mcmagi.savesync.save.SaveManager;
import com.mcmagi.savesync.client.model.ClientConfig;
import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.net.URISyntaxException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Collections;
import java.util.List;
import org.apache.commons.io.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.util.UriComponentsBuilder;

/**
 *
 * @author Michael C. Maggio
 */
public class SaveManagerHttpClient implements SaveManager
{
	@Autowired
	private ObjectMapper mapper;

	@Autowired
	private RestHttpClient restClient;

	@Autowired
	private ClientConfig config;

	private final DateFormat dateFormat = new SimpleDateFormat(SaveInfo.ISO8601_DATE_PATTERN);

	@Override
	public List<SaveInfo> getSaveInfo() throws SaveException
	{
		try (InputStream stream = restClient.get(getSavePath()))
		{
			return mapper.readValue(stream, new TypeReference<List<SaveInfo>>(){});
		}
		catch (JsonProcessingException ex)
		{
			throw new SaveException("Failed to parse server response; list of games expected", ex);
		}
		catch (IOException|RequestFailedException ex)
		{
			throw new SaveException("Failed obtain list of games from server", ex);
		}
	}

	@Override
	public void updateSavePackage(String gameId, FileData data) throws SaveException
	{
		try
		{
			InputStream responseStream = restClient.post(getGameURI(gameId),
					Collections.singletonMap("lastModified", dateFormat.format(data.getLastModified())),
					"file", data);
			IOUtils.closeQuietly(responseStream); // we don't care about the response in this case
		}
		catch (IOException|RequestFailedException ex)
		{
			throw new SaveException("Failed to send game state to server for " + gameId, ex);
		}
	}

	@Override
	public FileData getSavePackage(String gameId) throws SaveException
	{
		try
		{
			return restClient.getFile(getGameURI(gameId));
		}
		catch (IOException|RequestFailedException ex)
		{
			throw new SaveException("Failed to retrieve game state from server for " + gameId, ex);
		}
	}

	private URI getGameURI(String gameId)
	{
		return UriComponentsBuilder.fromUri(getSavePath())
				.pathSegment(gameId)
				.build().toUri();
	}

	public URI getSavePath()
	{
		URI uri = UriComponentsBuilder.fromUri(config.getServer())
				.pathSegment("save/")
				.build().toUri();

		try
		{
			// hack fix to add trailing slash
			return new URI(uri.toString() + "/");
		}
		catch (URISyntaxException ex)
		{
			throw new RuntimeException("could not build uri", ex);
		}
	}
}
