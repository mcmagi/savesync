package com.mcmagi.savesync.client.dao;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.mcmagi.savesync.profile.Profile;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;

/**
 * Loads a profile json file from within the classpath.
 * @author Michael C. Maggio
 */
public class ProfileDaoClasspath
{
	private static final String RESOURCE_DIR = "com/mcmagi/savesync/profile";

	private final ObjectMapper mapper = new ObjectMapper();
	private final String resourceDir;

	public ProfileDaoClasspath()
	{
		this.resourceDir = RESOURCE_DIR;
	}

	public ProfileDaoClasspath(String resourceDir)
	{
		this.resourceDir = resourceDir;
	}

	/**
	 * Loads and deserializes the profile.
	 * @param gameId Game identifier
	 * @return Profile config
	 * @throws IOException 
	 */
	public Profile loadProfile(String gameId)
		throws IOException
	{
		return mapper.readValue(getProfileResource(gameId), Profile.class);
	}

	/**
	 * Returns the profile file as an input stream.
	 * @param gameId Game identifier
	 * @return Input stream
	 * @throws FileNotFoundException 
	 */
	private InputStream getProfileResource(String gameId)
		throws FileNotFoundException
	{
		String resourceName = getResourceName(gameId);
		InputStream stream = this.getClass().getClassLoader().getResourceAsStream(resourceName);
		if (stream == null)
			throw new FileNotFoundException(resourceName);
		return stream;
	}

	/**
	 * Returns the file name and location given a game id.
	 * @param gameId Game identifier
	 * @return File name and location
	 */
	private String getResourceName(String gameId)
	{
		return resourceDir + "/" + gameId + ".json";
	}
}
