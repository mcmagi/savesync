package com.mcmagi.savesync.client;

/**
 *
 * @author Michael C. Maggio
 */
public class OptionsException extends Exception
{
	public OptionsException(String message)
	{
		super(message);
	}

	public OptionsException(String arg, String message)
	{
		super(arg + ": " + message);
	}
}
