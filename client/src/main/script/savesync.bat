@echo off

rem Location of savesync client jar
rem TODO: dynamically update the file version (or produce an artifact w/o the version)
set SAVESYNC_JAR=.\savesync-client-0.2-SNAPSHOT.one-jar.jar

rem Run savesync
%JAVA_HOME%\bin\java -jar "%SAVESYNC_JAR%" %*