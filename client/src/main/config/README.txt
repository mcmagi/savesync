The JSON configuration file in this directory defines the following:
 * server - The URL of the savesync server
 * games - The set of games on the local system and their locations

Each game entry is a gameId-path pair.  The game id is the "key" to the
game's profile as found under client/src/main/resources.  Additional
game profiles can be added by including them in the classpath.

The path is location of the game's state.  This is often the installation
location of the game for classic games.  For newer games, they are commonly
stored under your user profile: e.g. %USERPROFILE%\AppData\...\<game> on
Windows 7.